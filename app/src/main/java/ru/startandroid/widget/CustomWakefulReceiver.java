package ru.startandroid.widget;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * Created by djek-grif on 10/29/17.
 */

public class CustomWakefulReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "CustomWakefulReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "intent:" + intent);
    }
}
