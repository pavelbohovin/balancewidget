package ru.startandroid.widget;

/**
 * Created by djek-grif on 10/29/17.
 */


import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;


public class UpdateService extends Service {

    private static final String TAG = "UpdateService";

    private AsyncTask<Void, Void, String> updateTask;
    private String SERVER_LINK = "http://46.101.202.118/balance.php";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        updateDataFromServer();
        return super.onStartCommand(intent, flags, startId);
    }

    private void updateDataFromServer() {
        if (updateTask == null) {
            updateTask = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    InputStream stream = null;
                    HttpURLConnection connection = null;
                    String result = "";
                    try {
                        URL url = new URL(SERVER_LINK);
                        connection = (HttpURLConnection) url.openConnection();
                        // Timeout for reading InputStream arbitrarily set to 3000ms.
                        connection.setReadTimeout(3000);
                        // Timeout for connection.connect() arbitrarily set to 3000ms.
                        connection.setConnectTimeout(3000);
                        // For this use case, set HTTP method to GET.
                        connection.setRequestMethod("GET");
                        // Already true by default but setting just in case; needs to be true since this request
                        // is carrying an input (response) body.
                        connection.setDoInput(true);
                        // Open communications link (network traffic occurs here).
                        connection.connect();
                        int responseCode = connection.getResponseCode();
                        if (responseCode != HttpURLConnection.HTTP_OK) {
                            throw new IOException("HTTP error code: " + responseCode);
                        }
                        stream = connection.getInputStream();
                        if (stream != null) {
                            // Converts Stream to String with max length of 500.
                            result = readStream(stream, 500);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "", e);
                    } finally {
                        // Close Stream and disconnect HTTPS connection.
                        if (stream != null) {
                            try {
                                stream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                    return result;
                }

                @Override
                protected void onPostExecute(String result) {
                    updateWidget(result);
                    updateTask = null;
                }
            };
            updateTask.execute();
        }
    }

    public String readStream(InputStream stream, int maxReadSize) throws IOException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;
        StringBuffer buffer = new StringBuffer();
        while (((readSize = reader.read(rawBuffer)) != -1) && maxReadSize > 0) {
            if (readSize > maxReadSize) {
                readSize = maxReadSize;
            }
            buffer.append(rawBuffer, 0, readSize);
            maxReadSize -= readSize;
        }
        return buffer.toString();
    }

    private void updateWidget(String result) {
        if (!TextUtils.isEmpty(result)) {
            RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget);
            view.setTextViewText(R.id.balance_text, result.replace("\"", ""));
            ComponentName theWidget = new ComponentName(this, BalanceWidget.class);
            AppWidgetManager manager = AppWidgetManager.getInstance(this);
            manager.updateAppWidget(theWidget, view);
            Toast.makeText(this, "Balance updated", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Update result:" + result);
        } else {
            Toast.makeText(this, R.string.can_not_update, Toast.LENGTH_LONG).show();
        }
    }

}
